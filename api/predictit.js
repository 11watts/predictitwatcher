const axios = require('axios')
const BASE_URL = 'https://www.predictit.org/api/marketdata/'

const getMarketData = async id => {
    const { data } = await axios.get(`${BASE_URL}markets/${id}`)
    const contractsFormated = data.contracts.reduce(
        (acc, c, i) =>
            `${acc} \n ${i + 1}. ${c.name}: Last trade price is $${
                c.lastTradePrice
            }`,
        ''
    )
    return { ...data, contractsFormated }
}

const getMarketDataTime = async (id, maxContracts = 8) => {
    const {
        data,
    } = await axios.get(
        `https://www.predictit.org/api/Public/GetMarketChartData/${id}`,
        { params: { timespan: '24h', showHidden: true, maxContracts } }
    )
    return data
}

module.exports = { getMarketData, getMarketDataTime }
