require('dotenv').config()
const axios = require('axios')
const cron = require('node-cron')
const http = require('http')
const bodyParser = require('body-parser')
const express = require('express')
const Telegraf = require('telegraf')
const MessagingResponse = require('twilio').twiml.MessagingResponse
const api = require('./api/predictit')
const Users = require('./models/User')

const formatId = _id => {
    const id = _id.trim()
    if (id.length !== 4 || !/^\d+$/.test(id)) {
        throw new Error(`invaild id: ${_id}`)
    }
    return id
}

cron.schedule(process.env.SCHEDULE, () => {
    const users = Users().find({})
    console.log(`${users.length} users found`)
    users.every(u => {
        if (u.type === 'Telegram') {
            bot.telegram.sendMessage(u.id, `Your update for ${u.marketId}`)
        }
    })
})

const bot = new Telegraf(process.env.BOT_TOKEN)
bot.start(ctx => ctx.reply('Welcome'))
bot.command('watch', async ctx => {
    try {
        console.log(ctx.chat)

        const marketId = formatId(ctx.message.text.substring(7))
        const { id, username } = ctx.chat
        const data = await api.getMarketData(marketId)
        Users().insert({
            type: 'Telegram',
            id,
            username,
            marketId,
            key: `${id}${marketId}`,
        })
        ctx.reply(`Now watching ${data.name} (${marketId})`)
        ctx.reply(data.contractsFormated)
    } catch (error) {
        console.error(error)
        ctx.reply(`${error}`)
    }
})
bot.launch()

const app = express()

app.use(bodyParser.urlencoded({ extended: false }))

app.get('/all', async (req, res) => {
    try {
        const results = Users().find({})
        res.json({ done: true, results })
    } catch (error) {
        console.error(error)
        res.json({ error: error.toString() })
    }
})

app.get('/:id', async (req, res) => {
    try {
        const id = formatId(req.params.id)
        const data = await api.getMarketData(id)
        res.json({ id, data })
    } catch (error) {
        console.error(error)
        res.json({ error: error.toString() })
    }
})

app.post('/', async (req, res) => {
    try {
        const { Body } = req.body
        const id = Body.trim()
        const msg = await api.getMarketData(id)
        const twiml = new MessagingResponse()
        res.writeHead(200, { 'Content-Type': 'text/xml' })
        twiml.message(msg)
        res.end(twiml.toString())
    } catch (error) {
        console.error(error)
    }
})

http.createServer(app).listen(process.env.PORT, () => {
    console.log(`Express server listening on port ${process.env.PORT}`)
})
