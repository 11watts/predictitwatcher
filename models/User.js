const loki = require('lokijs')
let Users = { insert: () => null, find: () => null }
const databaseInitialize = () => {
    Users =
        db.getCollection('users') || db.addCollection('users', { unique: 'id-market' })
    const userCount = Users.count()
    console.log(`db loaded, userCount: ${userCount}`)
}

const db = new loki('users.db', {
    autoload: true,
    autoloadCallback: databaseInitialize,
    autosave: true,
    autosaveInterval: 4000,
})

module.exports = () => Users
