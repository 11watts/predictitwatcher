const diffOverTime = (data, th) => {
    const highSharePrices = data.map(i => i.highSharePrice)
    const max = Math.max(...highSharePrices)
    const min = Math.min(...highSharePrices)
    console.log(`max ${max}, min ${min}`)
    max - min > th
        ? console.log(`${data[0].contractName} passed th ${th}`)
        : console.log(`${data[0].contractName} DID NOT passed th ${th}`)
    return max - min > th
}

const convertToContracyArrays = data =>
    data.reduce((acc, curr) => {
        const currRows = acc[`${curr.marketId}-${curr.contractId}`] || []
        const a = {
            ...acc,
            [`${curr.marketId}-${curr.contractId}`]: [...currRows, curr],
        }
        return a
    }, {})

module.exports = { diffOverTime, convertToContracyArrays }
